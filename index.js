$(document).ready(function () {
  $('input[type="text"],select').each(function () {
    var keylocalstr = $(this).attr('name');
    var valuelocalstr = localStorage.getItem(keylocalstr);
    valuelocalstr = $(this).val(valuelocalstr);
  })
  setInterval(function () {
    $('input[type="text"],select').each(function () {
      keylocalstr = $(this).attr('name');
      valuelocalstr = $(this).val();
      localStorage.setItem(keylocalstr, valuelocalstr);
    })
  }, 1000);
})